#KAIST menubot

A Slack(slack.com) bot that serves daily menus from KAIST cafeterias. This project is forked from [Hongmoa](https://github.com/haandol/hongmoa).

## Dependencies

Inherited from Hongmoa:

* [gevent](https://github.com/gevent/gevent)
* [redis](https://github.com/andymccurdy/redis-py)
* [slackclient](https://github.com/slackhq/python-slackclient).

Required for menu crawling:

* [BeautifulSoup](http://www.crummy.com/software/BeautifulSoup/)

## Installation

1. Get a custom integration token from Slack and edit `settings.py`.

2. Install dependencies.
```bash
$ pip install -r requirements.txt
```

3. Run the bot.

```bash
$ python robot.py
 
```

## Usage

Try `!메뉴` or `!menu` once the bot is active in your channel.

## Extending the menubot

The original examples from [Hongmoa](https://github.com/haandol/hongmoa) are included in the `apps` directory for references. Please refer to [Hongmoa](https://github.com/haandol/hongmoa) documentations as well.